package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/go-resty/resty/v2"
)

// Posts ...
type Posts []struct {
	ID      int    `json:"id"`
	Date    string `json:"date"`
	DateGmt string `json:"date_gmt"`
	GUID    struct {
		Rendered string `json:"rendered"`
	} `json:"guid"`
	Modified    string `json:"modified"`
	ModifiedGmt string `json:"modified_gmt"`
	Slug        string `json:"slug"`
	Status      string `json:"status"`
	Type        string `json:"type"`
	Link        string `json:"link"`
	Title       struct {
		Rendered string `json:"rendered"`
	} `json:"title"`
	Content struct {
		Rendered  string `json:"rendered"`
		Protected bool   `json:"protected"`
	} `json:"content"`
	Excerpt struct {
		Rendered  string `json:"rendered"`
		Protected bool   `json:"protected"`
	} `json:"excerpt"`
	Author        int    `json:"author"`
	FeaturedMedia int    `json:"featured_media"`
	CommentStatus string `json:"comment_status"`
	PingStatus    string `json:"ping_status"`
	Sticky        bool   `json:"sticky"`
	Template      string `json:"template"`
	Format        string `json:"format"`
	Meta          struct {
		AmpStatus string `json:"amp_status"`
	} `json:"meta"`
	Categories []int         `json:"categories"`
	Tags       []interface{} `json:"tags"`
	Links      struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		About []struct {
			Href string `json:"href"`
		} `json:"about"`
		Author []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"author"`
		Replies []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"replies"`
		VersionHistory []struct {
			Count int    `json:"count"`
			Href  string `json:"href"`
		} `json:"version-history"`
		PredecessorVersion []struct {
			ID   int    `json:"id"`
			Href string `json:"href"`
		} `json:"predecessor-version"`
		WpFeaturedmedia []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"wp:featuredmedia"`
		WpAttachment []struct {
			Href string `json:"href"`
		} `json:"wp:attachment"`
		WpTerm []struct {
			Taxonomy   string `json:"taxonomy"`
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"wp:term"`
		Curies []struct {
			Name      string `json:"name"`
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"curies"`
	} `json:"_links"`
}

type client struct {
	c *resty.Client
	baseurl string
	timeNow string
}

func initClient() client{
	c := client{}
	c.c = resty.New()
	c.baseurl = "https://thetravelintern.com/wp-json/wp/v2/"
	c.timeNow = time.Now().Format("01-02-2006_15:04:05")

	return c
}

func main() {
	//c := initClient()
	//src := c.getPosts()

	// Read JSON file and get posts
	src := "07-22-2019_17:45:13_output.json"
	posts, err := parseJSONPosts(src)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Number of posts: %d\n", len(posts))

	fmt.Printf("Title of latest post: %s\n", posts[0].Title.Rendered)
}

func parseJSONPosts(src string) (Posts, error) {
	file, err := os.Open(src)
	if err != nil {
		return nil, fmt.Errorf("could not open file %s: %v", src, err)
	}
	defer file.Close()

	var posts Posts
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("could not read data from %s: %v", src, err)
	}
	if err := json.Unmarshal(data, &posts); err != nil {
		return nil, fmt.Errorf("could not unmarshall json from %s: %v", src, err)
	}
	return posts, nil
}

func (c *client) getPosts() string {
	filename := c.timeNow+"_output.json"
	resp, err := c.c.R().Get(c.baseurl+"posts")
	if err != nil {
		log.Fatal(err)
	}

	file, err := os.OpenFile(filename, os.O_CREATE | os.O_WRONLY, 0644 )
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	if _, err := file.Write(resp.Body()); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved latest 10 posts to %s\n", filename)
	return filename
}

