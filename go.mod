module github.com/zeddee/tti-rest

go 1.12

require (
	github.com/go-resty/resty/v2 v2.0.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190719005602-e377ae9d6386 // indirect
)
